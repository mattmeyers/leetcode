"""
Implement atoi which converts a string to an integer.

The function first discards as many whitespace characters as necessary until the first non-whitespace character is found. Then, starting from this character, takes an optional initial plus or minus sign followed by as many numerical digits as possible, and interprets them as a numerical value.

The string can contain additional characters after those that form the integral number, which are ignored and have no effect on the behavior of this function.

If the first sequence of non-whitespace characters in str is not a valid integral number, or if no such sequence exists because either str is empty or it contains only whitespace characters, no conversion is performed.

If no valid conversion could be performed, a zero value is returned.

Note:

   - Only the space character ' ' is considered as whitespace character.
   - Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. If the numerical value is out of the range of representable values, INT_MAX (231 − 1) or INT_MIN (−231) is returned.
"""

class Solution:
    # Could definitely be sped up by using RegEx
    def myAtoi(self, str):
        """
        :type str: str
        :rtype: int
        """
        _MAX_INT = (2**31)-1
        _MIN_INT = -(2**31)

        numChars = "+-1234567890"
        newStr = ""

        str = str.lstrip()

        for char in str:
            if (char in numChars):
                if (len(newStr) == 0):
                    newStr += char if char != "+" else ""
                    numChars = "1234567890"
                else:
                    newStr += char
            else:
                break
        
        if (len(newStr) == 0 or newStr == "-"):
            return 0

        newStr = int(newStr)
        if (newStr > _MAX_INT):
            return _MAX_INT
        elif (newStr < _MIN_INT):
            return _MIN_INT
        else:
            return newStr


if __name__ == "__main__":
    test1 = "42"
    test2 = "   -42"
    test3 = "4193 with words"
    test4 = "words and 987"
    test5 = "-91283472332"
    test6 = "+123"
    test7 = "-abc"
    test8 = "1-1"
    test9 = ""
    sol = Solution()
    assert(sol.myAtoi(test1) == 42)
    assert(sol.myAtoi(test2) == -42)
    assert(sol.myAtoi(test3) == 4193)
    assert(sol.myAtoi(test4) == 0)
    assert(sol.myAtoi(test5) == -2147483648)
    assert(sol.myAtoi(test6) == 123)
    assert(sol.myAtoi(test7) == 0)
    assert(sol.myAtoi(test8) == 1)
    assert(sol.myAtoi(test9) == 0)
    print("Great Success!")