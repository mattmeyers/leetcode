"""
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.
"""

class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        tmp = {}
        for i in range(0, len(nums)):
            diff = target-nums[i]
            if (diff in tmp):
                return [tmp[diff], i]
            else:
                tmp[nums[i]] = i

        return None

if __name__ == "__main__":
    nums1 = [1,7,3,4]
    target1 = 5
    nums2 = [-3,3,5,7]
    target2 = 0
    nums3 = [1,-4,5,-3,2,-5]
    target3 = -9
    nums4 = [1,-9,8,-7]
    target4 = -6
    sol = Solution()
    assert(sol.twoSum(nums1, target1) == [0,3])
    assert(sol.twoSum(nums2, target2) == [0,1])
    assert(sol.twoSum(nums3, target3) == [1,5])
    assert(sol.twoSum(nums4, target4) == [0,3])
    print("Great Success!")