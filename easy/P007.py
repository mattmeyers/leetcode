"""
Given a 32-bit signed integer, reverse digits of an integer.
"""

class Solution:
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        if (x < 0):
            rev =  (-1 * int(str(abs(x))[::-1]))
            return 0 if (rev < -(2**31)) else rev
        else:
            rev = int(str(x)[::-1])
            return 0 if rev > (2**31)-1 else rev

if __name__ == "__main__":
    test1 = 123
    test2 = -123
    test3 = 120
    test4 = 1534236469
    sol = Solution()
    assert(sol.reverse(test1) == 321)
    assert(sol.reverse(test2) == -321)
    assert(sol.reverse(test3) == 21)
    assert(sol.reverse(test4) == 0)
    print("Great Success!")